{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Network.Gitit.Paths_filestore (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,6,3,4] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/builder/hackage-server/build-cache/tmp-install/bin"
libdir     = "/home/builder/hackage-server/build-cache/tmp-install/lib/x86_64-linux-ghc-8.6.1/filestore-0.6.3.4-Ho3ut6dkwpvJCXNX06aLVC"
dynlibdir  = "/home/builder/hackage-server/build-cache/tmp-install/lib/x86_64-linux-ghc-8.6.1"
datadir    = "/home/builder/hackage-server/build-cache/tmp-install/share/x86_64-linux-ghc-8.6.1/filestore-0.6.3.4"
libexecdir = "/home/builder/hackage-server/build-cache/tmp-install/libexec/x86_64-linux-ghc-8.6.1/filestore-0.6.3.4"
sysconfdir = "/home/builder/hackage-server/build-cache/tmp-install/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "filestore_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "filestore_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "filestore_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "filestore_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "filestore_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "filestore_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)